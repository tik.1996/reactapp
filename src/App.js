import React, {Component} from 'react';
import Header from './components/layouts/Header/Header';
import One from './components/pages/One/One';
import Review from './components/Review/Review';
import {Route,NavLink} from 'react-router-dom'

export default class App extends Component {
    state = {
        link:1,
        nav: [
            { icon: 'fas fa-street-view',  text: '1) Setup Location', arrow: 'fas fa-angle-right', navlink: '/'},
            { icon: 'fa fa-poll-h',  text: '2) Enter Details', arrow: 'fas fa-angle-right', navlink: '/generaltwo'},
            { icon: 'fa fa-window-restore',  text: '3) Select Services', arrow: 'fas fa-angle-right', navlink: '/generalthree'},
            { icon: 'fa fa-bus',  text: '4) Delivery ', arrow: 'fas fa-angle-right'},
            { icon: 'fa fa-globe',  text: '5) Setup Location'},

        ],
        one:{
            addres: [
                {addresLine: 'Addres Line 1', valueName: 'Addres Line 1', entered: 'Please enter your Addres'},
                {addresLine: 'Addres Line 2', valueName: 'Addres Line 2', entered: 'Please enter your Addres'},
            ],
            title: 'Select your Services',
            inputs: [
                {nameTitle: 'Postcode', inpValue: '3000', codeTitle: 'Please enter your Postcode.'},
                {nameTitle: 'City', inpValue: 'Melbourne', codeTitle: 'Please enter your City.'},
                {nameTitle: 'State', inpValue: 'VIC', codeTitle: 'Please enter your Postcode.'},
            ],

            options: [ 'Afganistan', 'Albania', 'Algeri', 'Andora', 'Angola', 'Armenia', 'Argentina' ],
            nextStep: ['Previous', 'Next Step'],
        },

        count: 0,

        details: [
            {title: 'Package Details', value: 'Complete Workstation (Monitor, Computer, Keyboard & Mouse)',
                smallText: 'Please enter your Pakcage Details.'},
            {title: 'Package Weight in KG', value: '25',
                smallText: 'PleasPlease enter your Package Weight in KG.'},
            {title: 'Package Dimensions Package Width in CM', value: '110',
                smallText: 'Please enter your Package Width in CM.'},
            {title: 'Package Height in CM', value: '90',
                smallText: 'Please enter your Package Height in CM.'},
            {title: 'Package Length in CM', value: '150',
                smallText: 'Please enter your Package Length in CM.'}
        ],
        services: [
            {title: 'Delivery Type', option: 'Overnight Delivery (within 48 hours)', delivery: 'working day and night', express: 'sdvsd'},
            {title: 'Delivery Type', option: 'Express Delivery (within 5 working days)', delivery: 'dsvs', express: 'vsdv'},
            {title: 'Delivery Type', option: 'Basic Delivery (within 5-10 working days)', delivery: 'svdsv', express: '5555'}
        ],
        review: [
            {title: 'Current Address', textOne: 'Address Line 1', textTwo: 'Address Line 2', textThree: 'Melbourne 3000, VIC, Australia'},
            {title: 'Delivery Details', textOne: 'Package: Complete Workstation (Monitor, Computer, Keyboard & Mouse)', textTwo: 'Weight: 25kg', textThree: 'Dimensions: 110cm (w) x 90cm (h) x 150cm (L)'},
            {title: 'Delivery Service Type', textOne: 'Overnight Delivery with Regular Packaging', textTwo: 'Preferred Morning (8:00AM - 11:00AM) Delivery'},
            {title: 'Delivery Address', textOne: 'Address Line 1', textTwo: 'Address Line 2', textThree: 'Preston 3072, VIC, Australia'},

        ]
    };
    changeLink = () => {
        if (this.state.link === this.state.nav.length-1){
            this.setState({link:0})
        }
        else{
            this.setState({link:this.state.link+1})
        }

    };


    render() {
        return(
            <>
                <Header nav={this.state.nav}/>

                <Route
                    path={'/'}
                    exact
                    component={()=><One one={this.state.one} />}/>
                <Route
                    path={'/generaltwo'}
                    component={()=><Review two={this.state.review}/>}/>

                <footer>
                    <h1>esel henc nor avelacreci</h1>
                    <NavLink onClick={this.changeLink} exact to={this.state.nav[this.state.link].navlink} >Next</NavLink>
                </footer>
            </>
        )
    }
}

