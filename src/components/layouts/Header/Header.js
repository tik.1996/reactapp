import React from 'react'
import classes from "../../../App.css";
import Navbar from "../../Navbar/Navbar";

export default props => {
    return (
        <>
            <nav className={classes.navs}>
                <div className={classes.navbar}>
                    {props.nav.map((elem, index) => {
                        return(
                            <Navbar key={index} icon={elem.icon} text={elem.text} arrow={elem.arrow} navlink={elem.navlink} />
                        )
                    })}
                </div>
            </nav>


        </>
    )
}