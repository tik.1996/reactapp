import React from'react';
import classes from './Addres.css';

export default props => {
    return(
        <div className={classes.inputs}>
            <p> {props.addresLine} </p>
            <input type="text" value={props.valueName} />
            <p>
                <small> {props.entered} </small>
            </p>
        </div>
    )
}