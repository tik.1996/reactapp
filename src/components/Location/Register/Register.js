import React from 'react';
import classes from './Register.css';

export default props => {
    return(
        <div className={classes.reg}>
            <p> {props.nameTitle} </p>
            <input type="text" value={props.inpValue} />
            <p>
                <small> {props.codeTitle} </small>
            </p>
        </div>
    )
}