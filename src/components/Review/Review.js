import React from 'react';

export default props => {
    return(
        <>
            {props.two.map((elem,index)=>{
                return(
                    <div key={index}>
                        <h1>{elem.title}</h1>
                        <p>{elem.textOne}</p>
                    </div>
                )
            })}
        </>

    )
}