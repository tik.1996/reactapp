import React from 'react';
import classes from './Navbar.css';
import {NavLink} from "react-router-dom";

export default props => {
    return(
        <NavLink to={props.navlink} exact={props.navlink==='/'?true:false}>
            <div className={classes.generalList}>
                <div className={classes.faicon}>
                    <i className={props.icon}></i>
                    <p> {props.text} </p>
                </div>
                <div className={classes.arrow}>
                  <i className={props.arrow}></i>
                </div>
            </div>
        </NavLink>

    )
}