import React from 'react';
import classes from './Next.css'

export default props => {
    return(
        <div className={classes.next}>
            <a href=""> {props.next} </a>
        </div>
    )
}