import React from 'react';
import classes from "./One.css"
import Addres from "../../Location/Addres/Addres";
import Register from "../../Location/Register/Register";
import Option from "../../Location/Register/Option/Option";

export default props => {
    return(
        <div className={classes.secondary}>
            <hr className={classes.line}/>
            <div className={classes.locational}>
                <div className={classes.setup}>
                    <h3> {props.one.title} </h3>

                </div>
                {props.one.addres.map((elem, index) => {
                    return(
                        <Addres key={index} addresLine={elem.addresLine} valueName={elem.valueName} entered={elem.entered} />
                    )
                })}
                <div className={classes.register}>
                    {props.one.inputs.map((elem, index) => {
                        return(
                            <Register key={index} nameTitle={elem.nameTitle} inpValue={elem.inpValue} codeTitle={elem.codeTitle} />
                        )
                    })}
                    <div>
                        <select name="" id="country">
                            {props.one.options.map((elem, index) => {
                                return(
                                    <Option key={index} option={elem} />
                                )
                            })}
                        </select>
                    </div>
                </div>
                <hr/>
                {/*<div className={classes.previous}>*/}
                {/*    {props.one.nextStep.map((elem, index) => {*/}
                {/*        return(*/}
                {/*            <Next key={index} next={elem} />*/}
                {/*        )*/}
                {/*    })}*/}
                {/*</div>*/}
            </div>
        </div>
    )
}