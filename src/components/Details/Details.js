import React from 'react';

export default props => {
    return(
        <div>
            <p> {props.title} </p>
            <input type="text" value={props.value} />
            <p>
                <small> {props.smallText} </small>
            </p>
        </div>
    )
}